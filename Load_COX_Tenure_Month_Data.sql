SELECT DISTINCT * ,

CASE WHEN `Months of Performance Data` >2 THEN (IFNULL(TenureMonth1,0)+IFNULL(TenureMonth2,0)+IFNULL(TenureMonth3,0))/ 
((CASE WHEN TenureMonth1 IS NULL THEN 0 ELSE 1 END) +(CASE WHEN TenureMonth2 IS NULL THEN 0 ELSE 1 END)+
(CASE WHEN TenureMonth3 IS NULL THEN 0 ELSE 1 END)) ELSE NULL END AS `3 Months of Performance Data` 

,CASE WHEN `Months of Performance Data` >5 THEN (IFNULL(TenureMonth1,0)+IFNULL(TenureMonth2,0)+IFNULL(TenureMonth3,0)
+IFNULL(TenureMonth4,0)+IFNULL(TenureMonth5,0)+IFNULL(TenureMonth6,0))/ ((CASE WHEN TenureMonth1 IS NULL THEN 0 ELSE 1 END) 
+(CASE WHEN TenureMonth2 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth3 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth4 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth5 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth6 IS NULL THEN 0 ELSE 1 END)) ELSE NULL END AS `6 MONTH Performance` 

,CASE WHEN `Months of Performance Data` >8 THEN (IFNULL(TenureMonth1,0)+IFNULL(TenureMonth2,0)+IFNULL(TenureMonth3,0)
+IFNULL(TenureMonth4,0)+IFNULL(TenureMonth5,0)+IFNULL(TenureMonth6,0)+IFNULL(TenureMonth7,0)+IFNULL(TenureMonth8,0)
+IFNULL(TenureMonth9,0))/ ((CASE WHEN TenureMonth1 IS NULL THEN 0 ELSE 1 END) +(CASE WHEN TenureMonth2 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth3 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth4 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth5 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth6 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth7 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth8 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth9 IS NULL THEN 0 ELSE 1 END)) ELSE NULL END AS `9 MONTH Performance` 

,CASE WHEN `Months of Performance Data` >11 THEN (IFNULL(TenureMonth1,0)+IFNULL(TenureMonth2,0)+IFNULL(TenureMonth3,0)
+IFNULL(TenureMonth4,0)+IFNULL(TenureMonth5,0)+IFNULL(TenureMonth6,0)+IFNULL(TenureMonth7,0)+IFNULL(TenureMonth8,0)
+IFNULL(TenureMonth9,0)+IFNULL(TenureMonth10,0)+IFNULL(TenureMonth11,0)+IFNULL(TenureMonth12,0))/ 
((CASE WHEN TenureMonth1 IS NULL THEN 0 ELSE 1 END) +(CASE WHEN TenureMonth2 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth3 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth4 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth5 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth6 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth7 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth8 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth9 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth10 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth11 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth12 IS NULL THEN 0 ELSE 1 END)) 
ELSE NULL END AS `12 MONTH Performance` 

,CASE WHEN `Months of Performance Data` >2 THEN (IFNULL(TenureMonth1,0)+IFNULL(TenureMonth2,0)+IFNULL(TenureMonth3,0)
+IFNULL(TenureMonth4,0)+IFNULL(TenureMonth5,0)+IFNULL(TenureMonth6,0)+IFNULL(TenureMonth7,0)+IFNULL(TenureMonth8,0)
+IFNULL(TenureMonth9,0)+IFNULL(TenureMonth10,0)+IFNULL(TenureMonth11,0)+IFNULL(TenureMonth12,0) + IFNULL(TenureMonth13,0)
+IFNULL(TenureMonth14,0)+IFNULL(TenureMonth15,0)+IFNULL(TenureMonth16,0)+IFNULL(TenureMonth17,0)+IFNULL(TenureMonth18,0)
+IFNULL(TenureMonth19,0)+IFNULL(TenureMonth20,0)+IFNULL(TenureMonth21,0)+IFNULL(TenureMonth22,0)+IFNULL(TenureMonth23,0) 
+IFNULL(TenureMonth24,0)+IFNULL(TenureMonth25,0)+IFNULL(TenureMonth26,0)+IFNULL(TenureMonth27,0)+IFNULL(TenureMonth28,0)
+IFNULL(TenureMonth29,0)+IFNULL(TenureMonth30,0) +IFNULL(TenureMonth31,0)+IFNULL(TenureMonth32,0)+IFNULL(TenureMonth33,0)
+IFNULL(TenureMonth34,0)+IFNULL(TenureMonth35,0)+IFNULL(TenureMonth36,0))/ 
((CASE WHEN TenureMonth1 IS NULL THEN 0 ELSE 1 END) +(CASE WHEN TenureMonth2 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth3 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth4 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth5 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth6 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth7 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth8 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth9 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth10 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth11 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth12 IS NULL THEN 0 ELSE 1 END)
+ (CASE WHEN TenureMonth13 IS NULL THEN 0 ELSE 1 END) +(CASE WHEN TenureMonth14 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth15 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth16 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth17 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth18 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth19 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth20 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth21 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth22 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth23 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth24 IS NULL THEN 0 ELSE 1 END) 
+ (CASE WHEN TenureMonth25 IS NULL THEN 0 ELSE 1 END) +(CASE WHEN TenureMonth26 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth27 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth28 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth29 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth30 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth31 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth32 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth33 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth34 IS NULL THEN 0 ELSE 1 END)
+(CASE WHEN TenureMonth35 IS NULL THEN 0 ELSE 1 END)+(CASE WHEN TenureMonth36 IS NULL THEN 0 ELSE 1 END) ) 
ELSE NULL END AS `Average Performance`
FROM
(
SELECT DISTINCT EmployeeID,MetricAbbreviation,
MIN(Datamonth1) AS DataMonth1 ,MIN(Datamonth2) AS DataMonth2 ,MIN(Datamonth3) AS
DataMonth3
,MIN(Datamonth4) AS DataMonth4 ,MIN(Datamonth5) AS DataMonth5 ,MIN(Datamonth6) AS
DataMonth6 ,MIN(Datamonth7) AS DataMonth7
,MIN(Datamonth8) AS DataMonth8 ,MIN(Datamonth9) AS DataMonth9 ,MIN(Datamonth10) AS
DataMonth10 ,MIN(Datamonth11) AS DataMonth11
,MIN(Datamonth12) AS DataMonth12 ,MIN(Datamonth13) AS DataMonth13 ,MIN(Datamonth14) AS
DataMonth14 ,MIN(Datamonth15) AS DataMonth15
,MIN(Datamonth16) AS DataMonth16 ,MIN(Datamonth17) AS DataMonth17 ,MIN(Datamonth18) AS
DataMonth18 ,MIN(Datamonth19) AS DataMonth19
,MIN(Datamonth20) AS DataMonth20 ,MIN(Datamonth21) AS DataMonth21 ,MIN(Datamonth22) AS
DataMonth22 ,MIN(Datamonth23) AS DataMonth23
,MIN(Datamonth24) AS DataMonth24 ,MIN(Datamonth25) AS DataMonth25 ,MIN(Datamonth26) AS
DataMonth26 ,MIN(Datamonth27) AS DataMonth27
,MIN(Datamonth28) AS DataMonth28 ,MIN(Datamonth29) AS DataMonth29 ,MIN(Datamonth30) AS
DataMonth30 ,MIN(Datamonth31) AS DataMonth31
,MIN(Datamonth32) AS DataMonth32 ,MIN(Datamonth33) AS DataMonth33 ,MIN(Datamonth34) AS
DataMonth34 ,MIN(Datamonth35) AS DataMonth35
,MIN(Datamonth36) AS DataMonth36 ,MIN(Tenuremonth1) AS Tenuremonth1 ,MIN(Tenuremonth2)
AS Tenuremonth2 ,MIN(Tenuremonth3) AS Tenuremonth3
,MIN(Tenuremonth4) AS Tenuremonth4 ,MIN(Tenuremonth5) AS Tenuremonth5
,MIN(Tenuremonth6) AS Tenuremonth6 ,MIN(Tenuremonth7) AS Tenuremonth7
,MIN(Tenuremonth8) AS Tenuremonth8 ,MIN(Tenuremonth9) AS Tenuremonth9
,MIN(Tenuremonth10) AS Tenuremonth10
,MIN(Tenuremonth11) AS Tenuremonth11 ,MIN(Tenuremonth12) AS Tenuremonth12
,MIN(Tenuremonth13) AS Tenuremonth13
,MIN(Tenuremonth14) AS Tenuremonth14 ,MIN(Tenuremonth15) AS Tenuremonth15
,MIN(Tenuremonth16) AS Tenuremonth16
,MIN(Tenuremonth17) AS Tenuremonth17 ,MIN(Tenuremonth18) AS Tenuremonth18
,MIN(Tenuremonth19) AS Tenuremonth19
,MIN(Tenuremonth20) AS Tenuremonth20 ,MIN(Tenuremonth21) AS Tenuremonth21
,MIN(Tenuremonth22) AS Tenuremonth22
,MIN(Tenuremonth23) AS Tenuremonth23 ,MIN(Tenuremonth24) AS Tenuremonth24
,MIN(Tenuremonth25) AS Tenuremonth25
,MIN(Tenuremonth26) AS Tenuremonth26 ,MIN(Tenuremonth27) AS Tenuremonth27
,MIN(Tenuremonth28) AS Tenuremonth28 ,
MIN(Tenuremonth29) AS Tenuremonth29 ,MIN(Tenuremonth30) AS Tenuremonth30
,MIN(Tenuremonth31) AS Tenuremonth31
,MIN(Tenuremonth32) AS Tenuremonth32 ,MIN(Tenuremonth33) AS Tenuremonth33
,MIN(Tenuremonth34) AS Tenuremonth34
,MIN(Tenuremonth35) AS Tenuremonth35 ,MIN(Tenuremonth36) AS Tenuremonth36
,MIN(`Months of Performance Data`) AS `Months of Performance Data`
FROM (
SELECT DISTINCT
`EmployeeID`,`MetricAbbreviation`,
CASE WHEN MonthDiff=1 THEN MetricValue ELSE NULL END AS DataMonth1,
CASE WHEN MonthDiff=2 THEN MetricValue ELSE NULL END AS DataMonth2 ,
CASE WHEN MonthDiff=3 THEN MetricValue ELSE NULL END AS DataMonth3 ,
CASE WHEN MonthDiff=4 THEN MetricValue ELSE NULL END AS DataMonth4,
CASE WHEN MonthDiff=5 THEN MetricValue ELSE NULL END AS DataMonth5,
CASE WHEN MonthDiff=6 THEN MetricValue ELSE NULL END AS DataMonth6,
CASE WHEN MonthDiff=7 THEN MetricValue ELSE NULL END AS DataMonth7,
CASE WHEN MonthDiff=8 THEN MetricValue ELSE NULL END AS DataMonth8,
CASE WHEN MonthDiff=9 THEN MetricValue ELSE NULL END AS DataMonth9,
CASE WHEN MonthDiff=10 THEN MetricValue ELSE NULL END AS DataMonth10,
CASE WHEN MonthDiff=11 THEN MetricValue ELSE NULL END AS DataMonth11,
CASE WHEN MonthDiff=12 THEN MetricValue ELSE NULL END AS DataMonth12,
CASE WHEN MonthDiff=13 THEN MetricValue ELSE NULL END AS DataMonth13,
CASE WHEN MonthDiff=14 THEN MetricValue ELSE NULL END AS DataMonth14,
CASE WHEN MonthDiff=15 THEN MetricValue ELSE NULL END AS DataMonth15,
CASE WHEN MonthDiff=16 THEN MetricValue ELSE NULL END AS DataMonth16,
CASE WHEN MonthDiff=17 THEN MetricValue ELSE NULL END AS DataMonth17,
CASE WHEN MonthDiff=18 THEN MetricValue ELSE NULL END AS DataMonth18,
CASE WHEN MonthDiff=19 THEN MetricValue ELSE NULL END AS DataMonth19,
CASE WHEN MonthDiff=20 THEN MetricValue ELSE NULL END AS DataMonth20,
CASE WHEN MonthDiff=21 THEN MetricValue ELSE NULL END AS DataMonth21,
CASE WHEN MonthDiff=22 THEN MetricValue ELSE NULL END AS DataMonth22,
CASE WHEN MonthDiff=23 THEN MetricValue ELSE NULL END AS DataMonth23,
CASE WHEN MonthDiff=24 THEN MetricValue ELSE NULL END AS DataMonth24,
CASE WHEN MonthDiff=25 THEN MetricValue ELSE NULL END AS DataMonth25,
CASE WHEN MonthDiff=26 THEN MetricValue ELSE NULL END AS DataMonth26,
CASE WHEN MonthDiff=27 THEN MetricValue ELSE NULL END AS DataMonth27,
CASE WHEN MonthDiff=28 THEN MetricValue ELSE NULL END AS DataMonth28,
CASE WHEN MonthDiff=29 THEN MetricValue ELSE NULL END AS DataMonth29,
CASE WHEN MonthDiff=30 THEN MetricValue ELSE NULL END AS DataMonth30,
CASE WHEN MonthDiff=31 THEN MetricValue ELSE NULL END AS DataMonth31,
CASE WHEN MonthDiff=32 THEN MetricValue ELSE NULL END AS DataMonth32,
CASE WHEN MonthDiff=33 THEN MetricValue ELSE NULL END AS DataMonth33,
CASE WHEN MonthDiff=34 THEN MetricValue ELSE NULL END AS DataMonth34,
CASE WHEN MonthDiff=35 THEN MetricValue ELSE NULL END AS DataMonth35,
CASE WHEN MonthDiff=36 THEN MetricValue ELSE NULL END AS DataMonth36,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff THEN MetricValue ELSE NULL END AS
`TenureMonth1`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 1 THEN MetricValue ELSE NULL END AS
`TenureMonth2`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 2 THEN MetricValue ELSE NULL END AS
`TenureMonth3`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 3 THEN MetricValue ELSE NULL END AS
`TenureMonth4`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 4 THEN MetricValue ELSE NULL END AS
`TenureMonth5`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 5 THEN MetricValue ELSE NULL END AS
`TenureMonth6`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 6 THEN MetricValue ELSE NULL END AS
`TenureMonth7`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 7 THEN MetricValue ELSE NULL END AS
`TenureMonth8`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 8 THEN MetricValue ELSE NULL END AS
`TenureMonth9`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 9 THEN MetricValue ELSE NULL END AS
`TenureMonth10`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 10 THEN MetricValue ELSE NULL END AS
`TenureMonth11`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 11 THEN MetricValue ELSE NULL END AS
`TenureMonth12`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 12 THEN MetricValue ELSE NULL END AS
`TenureMonth13`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 13 THEN MetricValue ELSE NULL END AS
`TenureMonth14`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 14 THEN MetricValue ELSE NULL END AS
`TenureMonth15`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 15 THEN MetricValue ELSE NULL END AS
`TenureMonth16`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 16 THEN MetricValue ELSE NULL END AS
`TenureMonth17`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 17 THEN MetricValue ELSE NULL END AS
`TenureMonth18`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 18 THEN MetricValue ELSE NULL END AS
`TenureMonth19`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 19 THEN MetricValue ELSE NULL END AS
`TenureMonth20`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 20 THEN MetricValue ELSE NULL END AS
`TenureMonth21`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 21 THEN MetricValue ELSE NULL END AS
`TenureMonth22`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 22 THEN MetricValue ELSE NULL END AS
`TenureMonth23`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 23 THEN MetricValue ELSE NULL END AS
`TenureMonth24`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 24 THEN MetricValue ELSE NULL END AS
`TenureMonth25`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 25 THEN MetricValue ELSE NULL END AS
`TenureMonth26`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 26 THEN MetricValue ELSE NULL END AS
`TenureMonth27`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 27 THEN MetricValue ELSE NULL END AS
`TenureMonth28`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 28 THEN MetricValue ELSE NULL END AS
`TenureMonth29`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 29 THEN MetricValue ELSE NULL END AS
`TenureMonth30`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 30 THEN MetricValue ELSE NULL END AS
`TenureMonth31`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 31 THEN MetricValue ELSE NULL END AS
`TenureMonth32`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 32 THEN MetricValue ELSE NULL END AS
`TenureMonth33`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 33 THEN MetricValue ELSE NULL END AS
`TenureMonth34`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 34 THEN MetricValue ELSE NULL END AS
`TenureMonth35`,
CASE WHEN MonthDiff=FirstMonth+ TenureMonthDiff + 35 THEN MetricValue ELSE NULL END AS
`TenureMonth36`,
`CountMetricValue` AS `Months of Performance Data`
FROM `COX_Tenure_Stage_Data`) AS a GROUP BY a.EmployeeID,a.MetricAbbreviation) AS b