Select 
p.[FirstName],	p.[LastName],	p.[Email],	p.[EmpId],	p.[gender],	p.[age],	prj.[ProjectName],	pawsm.[PArticipantApplicationID],	pawsm.[jobworkflowstepid],	
pawsm.[controlOptionID],	pawsm.[moduleID],	ja.[JobApplicationName],	p.[ParticipantID],	ja.[jobapplicationID],	pawsm.[moduleName],	pawsm.[ControlName],	
pawsm.[ControlItemID],	pawsm.[controlOptionItem],	pawsm.[controlOptionValue],	pawsm.[QuestionID],	pawsm.[status],	pawsm.[notes],	pawsm.[createdby],	pawsm.[createddate],	
pawsm.[modifiedby],	pawsm.[Modifieddate]
FROM dbo.Participant p WITH(NOLOCK)
JOIN dbo.Project prj WITH(NOLOCK) ON p.ClientID = prj.ClientID and p.ProjectID=prj.ProjectID
JOIN dbo.ParticipantJobApplicationWorkflowStepModule pawsm WITH(NOLOCK) ON p.ParticipantID = pawsm.ParticipantID
JOIN dbo.JobApplication ja WITH(NOLOCK) ON ja.JobApplicationID = pawsm.JobApplicationID
WHERE p.ClientID=10289 
and pawsm.ModuleID=5799