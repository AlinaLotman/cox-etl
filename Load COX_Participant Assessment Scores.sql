SELECT DISTINCT 
ParticipantID, 
ParticipantApplicationID, 
Age,
Gender, 
EthnicityID, 
RecommendationLevel,
RecommendationTitle, 
AssessmentName, 
ScoringAlgorithmID, 
a_1.Completion,
JobName, 
ScaleID, 
ScaleName, 
RawScore, 
PercentileScore, 
AssessmentCreatedDate,
CASE WHEN PercentileScore > 0 THEN PercentileScore 
	 ELSE Rawscore 
END AS DisplayScore,
1 AS InParticipantAssessment,
CASE WHEN RecommendationLevel = 1 THEN 'NotRecommended' 
	 WHEN RecommendationLevel = 2 THEN 'Recommended'
	 WHEN RecommendationLevel = 3 THEN 'Highly Recommeneded' 
END AS AssessmentTier,
CASE WHEN Age = 1 THEN 'Male' 
	 WHEN Age = 2 THEN 'Female'
	 WHEN Age = 9 THEN 'Not Identified' ELSE NULL 
END AS AgeDisplayName

FROM 
( 
	SELECT DISTINCT 
	p.ParticipantID, 
	pA.ParticipantApplicationID, 
	p.Email,  
	p.Age, 
	p.Gender, 
	p.EthnicityID, 
	par.RecommendationLevel, 
	par.RecommendationTitle, 
	CASE WHEN a.AssessmentName = '_Typing_Test' THEN 'Typing Test' 
		 ELSE a.AssessmentName 
	END AS AssessmentName,
	paa.ScoringAlgorithmID,
	Case WHEN pa.Status=3 then 'Complete'
	     WHEN pa.Status=2 then 'Incomplete'
		 WHEN pa.Status=1 then 'Pending'
	END AS Completion,
	j.JobName, 
	paa.ScaleID, 
	Lm_Scale_Master.ScaleName, 
	CONVERT(numeric(10, 5), CASE WHEN isnumeric(RawScore + 'e0') = 1 OR isnumeric(RawScore + '.0e0') = 1 THEN RawScore ELSE NULL END) AS RawScore, 
	CONVERT(numeric(10, 5), 
	-- the Scores are stored in MIG as numeric fields, and bad assignments exist - this cases to numeric types 
	CASE WHEN isnumeric(PercentileScore + 'e0') = 1 OR 
	isnumeric(PercentileScore + '.0e0') = 1 THEN PercentileScore ELSE NULL END) AS PercentileScore, 
	--a.CreatedDate AS AssessmentCreatedDate
	paa.CreatedDate AS AssessmentCreatedDate
	-- current version has DisplayScore, which is PercentileScore (if it exists) else RawScore 
	FROM  dbo.Participant AS P WITH(NOLOCK)
		INNER JOIN dbo.ParticipantApplication AS PA 
		ON p.ParticipantID = PA.ParticipantID --Pa.ParticipantApplicationID = .ParticipantApplicationID 
		INNER JOIN dbo.ParticipantJob PJ 
		ON pa.ParticipantApplicationID=PJ.ParticipantApplicationID
		LEFT JOIN 
		dbo.ParticipantApplicationAssessment AS paa 
		on paa.ParticipantApplicationID=pa.ParticipantApplicationID 
		LEFT OUTER JOIN 
		( 
		-- This Logic is to used to assign the correct ScaleName for numeric Codes in ParticipantApplicationAssessment 
		SELECT 
		ScaleID, 
		ScaleName 
		FROM dbo.ScoringAlgorithmScale 
		UNION 
		SELECT CAST(ScaleID AS VARCHAR) AS Expr1, ScaleName 
		FROM dbo.Code_AuditionScale 
		UNION 
		SELECT CAST(ScaleID AS VARCHAR) AS Expr1, 
		ScaleName
		FROM dbo.Code_CCACallScale) AS Lm_Scale_Master ON Lm_Scale_Master.ScaleID = paa.ScaleID 
		-- JobName lookup 
		LEFT OUTER JOIN dbo.Job AS J ON paa.JobID = J.JobID 
		LEFT OUTER JOIN dbo.JobAssessment AS JA ON j.JobID = ja.JobID 


		LEFT OUTER JOIN 
		-- AssessmentName Lookup 
		dbo.Assessment AS a ON paa.AssessmentID = a.AssessmentID 
		LEFT OUTER JOIN
		dbo.AssessmentForm AS af ON a.AssessmentID = af.AssessmentID and paa.FormID = af.FormID 
		LEFT OUTER JOIN 
		-- Recommendation table 
		dbo.ParticipantApplicationRecommendation AS par 
		ON paa.JobID = par.JobID AND paa.ParticipantApplicationID = par.ParticipantApplicationID AND a.AssessmentID = par.AssessmentID 

	-- Cox's ClientID is 10289 
	WHERE (p.ClientID = '10289') 
								) AS a_1
