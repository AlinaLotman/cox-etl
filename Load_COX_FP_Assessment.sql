SELECT DISTINCT
`Employee Number`,	
CAST(`Candidate Identifier` AS UNSIGNED)`Candidate Identifier`,	
`Name`,	
`Address`,	
`City`,	`State`,	
`Zip Code`,	
`Email`,	
`Gender`,	
`Race`,	
`Ethnicity`,	
CAST(`Submission Identifier` AS UNSIGNED)`Submission Identifier`,	
`Submission Source Type`,	
`Submission Source (BL)`,	
`Current Step Name`,	`Current Status Name`,	
CASE WHEN STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') IS NULL THEN DATE_FORMAT(`Submission Completed Date`, '%Y-%m-%d') 
ELSE STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') 
END AS `Submission Completed Date`,	
`Template Job Code`,	
`Level 1 - Name`,	
`Organization Level2 - Name`,	
`Organization Level3 - Name`	
-- ,CASE WHEN STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') IS NULL THEN DATE_FORMAT(`Submission Completed Date`, '%Y-%m-%d') 
-- ELSE STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') END AS `SubmissionCompletedDate`
FROM `Stage_COX_EmployeeAssessment`
WHERE FileName <> 'FurstPerson Assessments June 2015-January 2017.xlsx'

UNION

SELECT DISTINCT
`Employee Number`,	
CAST(`Candidate Identifier` AS UNSIGNED)`Candidate Identifier`,	
`Name`,	
`Address`,	
`City`,	
`State`,	
`Zip Code`,	
`Email`,	
`Gender`,	
`Race`,	
`Ethnicity`,	
CAST(`Submission Identifier` AS UNSIGNED)`Submission Identifier`,	
`Submission Source Type`,	
`Submission Source (BL)`,	
`Current Step Name`,	
`Current Status Name`,	
CASE WHEN STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') IS NULL THEN DATE_FORMAT(`Submission Completed Date`, '%Y-%m-%d') 
ELSE STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') 
END AS `Submission Completed Date`,	
`Template Job Code`,	`Level 1 - Name`,	`Organization Level2 - Name`,	`Organization Level3 - Name`
-- ,CASE WHEN STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') IS NULL THEN DATE_FORMAT(`Submission Completed Date`, '%Y-%m-%d') 
-- ELSE STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') END AS `SubmissionCompletedDate`
FROM `Stage_COX_EmployeeAssessment`
WHERE FileName='FurstPerson Assessments June 2015-January 2017.xlsx'
AND STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y') >= '2015-06-22' AND  STR_TO_DATE(`Submission Completed Date`, '%m/%d/%Y')<='2016-01-10'