SELECT DISTINCT
`Employee ID`,	
`Name`,	
`Job Code`,	
`Job Title`,	
`Full/Part Time Desc`,	
`FLSA Status Desc`,	
CAST(`Supervisor ID` AS UNSIGNED) `Supervisor ID`,	
`Supervisor Name`,	
CAST(`Oracle Department Number` AS UNSIGNED) `Oracle Department Number`,	
CAST(`Dept Code` AS UNSIGNED) `Dept Code`,	
`Dept Desc`,	
CAST(`LOB` AS UNSIGNED)`LOB`,	
`RBU`,	
`Business Unit Code`,	
`Business Unit Descr`,	
`Job Family Cd`,	
`Job Family Desc`,	
`Job Sub Family Cd`,	
`Job Sub Family Desc`,	
CASE 
WHEN LENGTH(LTRIM(RTRIM(`Hire Date`)))BETWEEN 8 AND 10 AND SUBSTRING(`Hire Date`,1,10) NOT LIKE '%-%'
THEN STR_TO_DATE((CONCAT(RIGHT(`Hire Date`,4),'/', SUBSTRING(`Hire Date`,1,LOCATE('/',`Hire Date`,1)-1),'/',SUBSTRING(SUBSTRING_INDEX(`Hire Date`,'/',2),LOCATE('/',SUBSTRING_INDEX(`Hire Date`,'/',2))+1))),'%Y/%m/%d')
ELSE STR_TO_DATE(REPLACE(`Hire Date`,'-','/'),'%Y/%m/%d') 
END AS `Hire Date`,	
CASE WHEN LENGTH(LTRIM(RTRIM(`Rehire Date`)))BETWEEN 8 AND 10 AND SUBSTRING(`Rehire Date`,1,10) NOT LIKE '%-%'
THEN STR_TO_DATE((CONCAT(RIGHT(`Rehire Date`,4),'/', SUBSTRING(`Rehire Date`,1,LOCATE('/',`Rehire Date`,1)-1),'/',SUBSTRING(SUBSTRING_INDEX(`Rehire Date`,'/',2),LOCATE('/',SUBSTRING_INDEX(`Rehire Date`,'/',2))+1))),'%Y/%m/%d')
ELSE STR_TO_DATE(REPLACE(`Rehire Date`,'-','/'),'%Y/%m/%d') 
END AS `Rehire Date`,	
CASE WHEN LENGTH(LTRIM(RTRIM(`Most Recent Hire Dt`)))BETWEEN 8 AND 10 AND SUBSTRING(`Most Recent Hire Dt`,1,10) NOT LIKE '%-%'
THEN STR_TO_DATE((CONCAT(RIGHT(`Most Recent Hire Dt`,4),'/', SUBSTRING(`Most Recent Hire Dt`,1,LOCATE('/',`Most Recent Hire Dt`,1)-1),'/',SUBSTRING(SUBSTRING_INDEX(`Most Recent Hire Dt`,'/',2),LOCATE('/',SUBSTRING_INDEX(`Most Recent Hire Dt`,'/',2))+1))),'%Y/%m/%d')
ELSE STR_TO_DATE(REPLACE(`Most Recent Hire Dt`,'-','/'),'%Y/%m/%d') 
END AS `Most Recent Hire Dt`,	
`Years Of Service`,	`Service Range Desc`,	-- `Job Entry Date`,	
CASE WHEN LENGTH(LTRIM(RTRIM(`Job Entry Date`)))BETWEEN 8 AND 10 AND SUBSTRING(`Job Entry Date`,1,10) NOT LIKE '%-%'
THEN STR_TO_DATE((CONCAT(RIGHT(`Job Entry Date`,4),'/', SUBSTRING(`Job Entry Date`,1,LOCATE('/',`Job Entry Date`,1)-1),'/',SUBSTRING(SUBSTRING_INDEX(`Job Entry Date`,'/',2),LOCATE('/',SUBSTRING_INDEX(`Job Entry Date`,'/',2))+1))),'%Y/%m/%d')
ELSE STR_TO_DATE(REPLACE(`Job Entry Date`,'-','/'),'%Y/%m/%d') 
END AS `Job Entry Date`,
`Time in Job (days)`,	
`Location Code`,	
`Location Desc`,	
`Location Address1`,	
`Location Address2`,	
`Location City`,	
`Location State`,	
`Location Zip Cd`,	
`EE Status Desc`,	
CASE WHEN LENGTH(LTRIM(RTRIM(`Termination Date`)))BETWEEN 8 AND 10 AND SUBSTRING(`Termination Date`,1,10) NOT LIKE '%-%'
THEN STR_TO_DATE((CONCAT(RIGHT(`Termination Date`,4),'/', SUBSTRING(`Termination Date`,1,LOCATE('/',`Termination Date`,1)-1),'/',SUBSTRING(SUBSTRING_INDEX(`Termination Date`,'/',2),LOCATE('/',SUBSTRING_INDEX(`Termination Date`,'/',2))+1))),'%Y/%m/%d')
ELSE STR_TO_DATE(REPLACE(`Termination Date`,'-','/'),'%Y/%m/%d') 
END AS `Termination Date`,	
CAST(`Termination Month` AS UNSIGNED)`Termination Month`,	
`Action Code`,	
`Reason Desc`,	
`Vol/Invol`,	
`Term Date Minus Hire Date`,	
`Quick Quits`,	
CAST(`Age` AS UNSIGNED)`Age`,	
`Gender Desc`,	
`Generation Name`,	
`Ethnicity Desc`,	
CAST(`L5 Employee ID` AS UNSIGNED)`L5 Employee ID`,	`L5 Name`,	
CAST(`L5` AS UNSIGNED)`L5`,	`L6 Employee ID`,	
`L6 Name`,	CAST(`L6` AS UNSIGNED)`L6`,	
`L7 Employee ID`,	`L7 Name`,	CAST(`L7` AS UNSIGNED)`L7`,	`L8 Employee ID`,	`L8 Name`,	
CAST(`L8` AS UNSIGNED)`L8`,	`L9 Employee ID`,	`L9 Name`,	
CAST(`L9` AS UNSIGNED)`L9`,	`L10 Employee ID`,	`L10 Name`,	CAST(`L10` AS UNSIGNED)`L10`,	`L11 Employee ID`,	
`L11 Name`,	CAST(`L11` AS UNSIGNED)`L11`,	
CASE WHEN LENGTH(LTRIM(RTRIM(`Effective Date`)))BETWEEN 8 AND 10 AND SUBSTRING(`Effective Date`,1,10) NOT LIKE '%-%'
THEN STR_TO_DATE((CONCAT(RIGHT(`Effective Date`,4),'/', SUBSTRING(`Effective Date`,1,LOCATE('/',`Effective Date`,1)-1),'/',SUBSTRING(SUBSTRING_INDEX(`Effective Date`,'/',2),LOCATE('/',SUBSTRING_INDEX(`Effective Date`,'/',2))+1))),'%Y/%m/%d')
ELSE STR_TO_DATE(REPLACE(`Effective Date`,'-','/'),'%Y/%m/%d') 
END AS `Effective Date`	
FROM `Stage_COX_EmployeeFileTerms`