SELECT DISTINCT ParticipantID
,RequisitionNumber
,RequisitionTitle
,OrderID
,CreatedDate
,ApplicationSourceType
,ApplicationSource
,CandidateID AS TaleoID
,Gender
,Race
,Ethnicity
,EmployeeId AS EmployeeID
FROM [dbo].[TaleoOrderReq]
WHERE CustomerId = 'COX'